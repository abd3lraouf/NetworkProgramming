# Network Programming section

## Section 01

### How to run section program

Steps :

1. download file to desktop
1. open terminal
2. change directory to desktop `cd ~/Desktop`
1. extract archive `tar -xf src.tar`
2. cd into dir `cd src`
3. compile client and save as client `g++ client.cpp -o client`

        -o determines the output of the compiler
4. compile server `g++ server.cpp -o server`
3. right click on terminal and choose `Open Terminal` a new terminal opens.
5. run the server first `./server` on one terminal.
1. run the client after that `./client 127.0.0.1` on the other terminal.


### Changes I made and some fixes :

1. added constant `PORT_NUM 1234` to `unp.h` to represent the port number.
1. On unix systems, you can't use ports `< 1023` without the `root` user permission so I changed that to port number `1234` in unp.h and used the constant `PORT_NUM` in both the server and client.
1. The most **important note** is : port numbers in both client and server **MUST MATCH** to be able to establish connections.

**PS** : No `root` permission is required as `PORT_NUM > 1023`

### Where are files :

the `src.tar` is included within the repo and also extracted

#### Good luck

Thanks Eng.El Hosseiny

